<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class DatabaseCreateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command creates a new database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        if (!env('DB_DATABASE', false)) {
            $this->info('Skipping creation of database as env(DB_DATABASE) is empty');
            return;
        } elseif (!file_exists(env('DB_DATABASE'))) {
            touch(env('DB_DATABASE'));
            $this->info('Created database and ran migrations');
            Artisan::call('migrate');
        }


        return 0;
    }
}
