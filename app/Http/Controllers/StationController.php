<?php

namespace App\Http\Controllers;

use App\Models\Station;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Env;
use Illuminate\Support\Facades\Http;

class StationController extends Controller
{

    private function get_and_save_all_stations()
    {
        $url = 'https://gateway.apiportal.ns.nl/places-api/v2/places?';
        $url .= http_build_query([
                'type' => 'stationV2',
                'limit' => 1500,
                'radius' => 1000,
                'lang' => 'en',
                'screen-density' => 'ios-2.0',
                'details' => false,
            ]
        );

        $response = Http::withHeaders(['Ocp-Apim-Subscription-Key' => Env::get('NS_API_KEY')])->get($url);

        $stations = $response->json()['payload'][0]['locations'];

        foreach ($stations as $station) {
            Station::upsert(
                [
                    'name' => $station['name'],
                    'stationCode' => $station['stationCode'],
                    'lat' => $station['lat'],
                    'lng' => $station['lng'],
                    'open' => $station['open'],
                    'code' => $station['code'],
                    'land' => $station['land'],
                    'UICCode' => $station['UICCode'],
                    'EVACode' => $station['EVACode'],
                    'json' => json_encode($station),
                ],
                ['stationCode'],
                ['name', 'lat', 'lng', 'open', 'code', 'land', 'UICCode', 'EVACode', 'json']
            );
        }

    }

    private function get_station_info(string $station_code, string $type = 'arrivals'): array
    {
        $url = 'https://gateway.apiportal.ns.nl/reisinformatie-api/api/v2/' . $type . '?';
        $url .= http_build_query(['station' => $station_code]);

        $response = Http::withHeaders(['Ocp-Apim-Subscription-Key' => Env::get('NS_API_KEY')])->get($url);

        $return = [];

        $items = $response->json()['payload'][$type];

        foreach ($items as $item) {
            if ($this->calculate_time_diff_in_hours($item['plannedDateTime'], 9) <= 1) {
                $return[$item['plannedDateTime']] = $item;
            }
        }

        ksort($return);

        return $return;
    }

    /**
     * @param string $time
     * @param int $round
     * @return float Time difference in hours
     */
    private function calculate_time_diff_in_hours(string $time, int $round = 0): float
    {

        $now = new DateTime('now');
        try {
            $time = new DateTime((string)$time);
        } catch (\Exception $e) {
            return -1;
        }
        $diff = $now->diff($time);

        $daysInSecs = $diff->format('%r%a') * 24 * 60 * 60;
        $hoursInSecs = $diff->h * 60 * 60;
        $minutesInSecs = $diff->i * 60;

        $diff_in_seconds = $daysInSecs + $hoursInSecs + $minutesInSecs + $diff->s;


        return round($diff_in_seconds / 60.0 / 60.0, $round);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $stations_count = Station::count();

        if ($stations_count == 0) {
            $this->get_and_save_all_stations();
        }

        $items_per_page = 10;

        $stations = Station::orderBy('stationCode')->paginate($items_per_page);

        return view('stations.index', compact('stations'))
            ->with('i', (request()->input('page', 1) - 1) * $items_per_page);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Station $station
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(Station $station)
    {
        $arrivals = $this->get_station_info($station->stationCode);
        $departures = $this->get_station_info($station->stationCode, 'departures');

        return view('stations.show', compact('station', 'arrivals', 'departures'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Station $station
     * @return \Illuminate\Http\Response
     */
    public function edit(Station $station)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Station $station
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Station $station)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Station $station
     * @return \Illuminate\Http\Response
     */
    public function destroy(Station $station)
    {
        //
    }
}
