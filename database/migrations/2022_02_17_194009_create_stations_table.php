<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stations', function (Blueprint $table) {
            $table->primary('stationCode');
            $table->string('stationCode', 6);
            $table->string('name')->nullable();
            $table->decimal('lat', 10, 6)->nullable();
            $table->decimal('lng', 10, 6)->nullable();
            $table->string('open', 7)->nullable();
            $table->string('code', 6)->nullable();
            $table->string('land', 2)->nullable();
            $table->string('UICCode', 7)->nullable();
            $table->string('EVACode', 7)->nullable();
            $table->json('json')->nullable();
            $table->timestamps();

//            $table->index(['stationCode']);

            $table->string('stationCode', 6)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stations');
    }
};
