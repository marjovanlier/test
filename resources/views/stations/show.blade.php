@extends('stations.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h1>{{ $station->name }} Station</h1>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('stations.index') }}"> Back</a>
            </div>
        </div>
    </div>
    <br>
    @foreach (['arrivals', 'departures'] as $type)
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h3>{{ucfirst($type)}}</h3>
                </div>
            </div>
        </div>
        <table class="table table-bordered">
            <tr>
                <th>Origin</th>
                <th>Name</th>
                <th>Planned Date Time</th>
            </tr>
            @foreach ($$type as $item)
                <?php $date = new DateTime($item['plannedDateTime']);?>
                <tr>
                    <td>{{ $item['origin'] ?? $item['direction']  }}</td>
                    <td>{{ $item['name'] }}</td>
                    <td>{{ $date->format('Y-m-d H:i:s') }}</td>
                </tr>
            @endforeach
        </table>
        <br>
    @endforeach
@endsection
