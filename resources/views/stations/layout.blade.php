<!DOCTYPE html>
<html>
<head>
    <title>Stations Test</title>
    <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet">
</head>
<body>

<div class="container">
    @yield('content')
</div>

</body>
</html>
