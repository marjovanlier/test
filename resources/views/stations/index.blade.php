@extends('stations.layout')

@section('content')
    <h1>Stations</h1>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Station Code</th>
            <th>Name</th>
            <th width="150px">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($stations as $station)
            <tr>
                <td>{{ $station->stationCode  }}</td>
                <td>{{ $station->name }}</td>
                <td>
                    <div class="text-center">
                        <a class="btn btn-info" href="{{ route('stations.show', $station->stationCode) }}">
                            Select
                        </a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {!! $stations->links() !!}



@endsection
